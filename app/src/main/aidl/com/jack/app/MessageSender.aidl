// MessageSender.aidl
package com.jack.app;

import com.jack.app.model.AidlMessage;

interface MessageSender {

    void sendMessage(in AidlMessage message);
}
