// WebServer.aidl
package com.jack.app;

// Declare any non-default types here with import statements

interface WebServer {

    void setUrl(in String url);
}
