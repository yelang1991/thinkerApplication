package com.jack.app.h5

import android.content.Context
import android.content.Intent
import android.util.Log
import android.webkit.JavascriptInterface
import com.jack.app.SecondActivity
import org.jetbrains.anko.runOnUiThread

/**
 *  js调用的实现
 *
 * @author Jack 2017-12-23 16:38
 */
class JsBrideImpl(context: Context) : JsBride {
    private val mContext = context

    @JavascriptInterface
    override fun startActivity() {
        mContext.runOnUiThread {
            val intent = Intent(mContext, SecondActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    @JavascriptInterface
    override fun putData(data: String?) {
        mContext.runOnUiThread {
            Log.e("DATA", data)
        }
    }
}