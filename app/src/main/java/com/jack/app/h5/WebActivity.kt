package com.jack.app.h5

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jack.app.R

/**
 * d
 *
 * @author Jack 2017-12-23 16:30
 */
class WebActivity : AppCompatActivity() {
    companion object {
        val KEY_URL = "url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().add(R.id.frame_container,
                WebFragment.newInstance(intent.getStringExtra(KEY_URL)))
                .commitAllowingStateLoss()
    }
}