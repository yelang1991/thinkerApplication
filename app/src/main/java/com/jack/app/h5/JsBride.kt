package com.jack.app.h5

/**
 * h5调用接口
 *
 * @author Jack 2017-12-23 16:37
 */
interface JsBride {

    fun startActivity()

    fun putData(data: String?)
}