package com.jack.app.h5

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.jack.app.WebServer

/**
 * h5的服务端
 *
 * @author Jack 2017-12-23 17:15
 */
class WebService : Service() {
    private val binder = object: WebServer.Stub() {
        override fun setUrl(url: String?) {
            val intent = Intent(applicationContext, WebActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(WebActivity.KEY_URL, "$url")
            startActivity(intent)
        }
    }

    override fun onBind(p0: Intent?): IBinder {
        return binder
    }
}