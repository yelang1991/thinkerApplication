package com.jack.app.h5

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.jack.app.R
import kotlinx.android.synthetic.main.fragment_webview.*

/**
 *
 * @author Jack 2017-12-23 16:22
 */
class WebFragment : Fragment() {

    companion object {
        fun newInstance(url: String?): WebFragment {
            val fragment = WebFragment()
            val bundle = Bundle()
            bundle.putString(WebActivity.KEY_URL, "$url")
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_webview, container, false)
    }

    @SuppressLint("JavascriptInterface")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val url = arguments[WebActivity.KEY_URL]
        webview.loadUrl("$url")
        // 支持js调用
        webview.settings.javaScriptEnabled = true
        webview.addJavascriptInterface(JsBrideImpl(activity), "app")
        webview.webChromeClient = object: WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progressbar.progress = newProgress
            }
        }

        webview.webViewClient = object: WebViewClient() {

        }
    }
}