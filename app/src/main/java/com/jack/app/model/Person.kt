package com.jack.app.model

/**
 *
 * @author Jack 2017-12-14 17:50
 */
data class Person(var name: String,var age: Int)