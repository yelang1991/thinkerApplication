package com.jack.app.model

import android.os.Parcel
import android.os.Parcelable

/**
 * 消息模型
 *
 * @author Jack 2017-12-21 17:28
 */
data class AidlMessage(var content: String? = null,
                       var from: String? = null,
                       var to: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(content)
        parcel.writeString(from)
        parcel.writeString(to)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AidlMessage> {
        override fun createFromParcel(parcel: Parcel): AidlMessage {
            return AidlMessage(parcel)
        }

        override fun newArray(size: Int): Array<AidlMessage?> {
            return arrayOfNulls(size)
        }
    }


}