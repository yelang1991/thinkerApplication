package com.jack.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 *
 *
 * @author Jack 2017-12-11 10:57
 */
class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.add(R.id.frame_container, PreviewFragment())
                   .commitAllowingStateLoss()
    }
}