package com.jack.app

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.jack.app.model.AidlMessage

/**
 *
 *
 * @author Jack 2017-12-21 17:43
 */
class MessageService : Service() {
    private val sender = object: MessageSender.Stub() {
        override fun sendMessage(message: AidlMessage?) {
            Log.e("message","${message?.content}")
        }

    }

    override fun onBind(p0: Intent?): IBinder? {
        return sender
    }
}