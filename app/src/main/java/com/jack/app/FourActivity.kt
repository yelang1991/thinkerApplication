package com.jack.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.coroutines.experimental.*


/**
 *
 *
 * @author Jack 2018-01-17 15:42
 */
class FourActivity : AppCompatActivity() {
    private val TAG = "tag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        test()
    }

    fun test() = runBlocking {
        val test1 = async(coroutineContext) {
            Log.e(TAG, "2")
        }

        val test2 = launch(coroutineContext) {
            Log.e(TAG, "1")
            delay(300)
            test1.join()
            Log.e(TAG, "233333")
        }
    }
}