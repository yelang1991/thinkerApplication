package com.jack.app

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import com.jack.app.h5.WebService
import com.jack.app.model.Person
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

/**
 *
 * @author Jack 2017-12-11 10:56
 */
class MainActivity : AppCompatActivity() {
    private var mConnection: ServiceConnection? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mConnection = object: ServiceConnection {
            override fun onServiceDisconnected(p0: ComponentName?) {

            }

            override fun onServiceConnected(p0: ComponentName?, binder: IBinder?) {
               val server = WebServer.Stub.asInterface(binder)
                server.setUrl("file:///android_asset/hello.html")
            }
        }
        text.setOnClickListener {
            val intent = Intent(this, WebService::class.java)
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
        }

        val person = Person("jack", 20)
        val personList = ArrayList<Person>()
        personList.add(person)
        for((a,b) in personList) {
            toast("$a<---->$b")
        }
    }

    private fun res(arg1: Int, arg2: Int, max: (arg1: Int, arg2: Int) ->Boolean): Int {
        return if (max(arg1, arg2)) arg1 else arg2
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(mConnection)
    }
}