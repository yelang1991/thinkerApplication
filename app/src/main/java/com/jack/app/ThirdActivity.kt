package com.jack.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 *
 * @author Jack 2017-12-19 16:25
 */
class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }
}